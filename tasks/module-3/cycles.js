/** Task: 1
 * implement factorial algorithm using for, while, do..while operators
 * assign the result to corresponding variable
 * for -> forFactorial: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
 * while -> whileFactorial: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/while
 * do..while -> doFactorial: https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/do...while
 */
let forFactorial, whileFactorial, doFactorial;
let n = 10; // 10! = 3628800
let memory = 1;
for ( let i=1; i<=10; i++ ) {

  forFactorial = memory * i;
  memory = forFactorial;
}
let memory2 = 1;
let m = 10;
while (m>1) {
  whileFactorial = memory2 * m;
  memory2 = whileFactorial;
  m = m - 1;
}
let memory3 = 1;
let k = 10;
do {
	doFactorial = memory3 * k;
  memory3 = doFactorial;
  k = k - 1;
} while (k > 1);

/** Task: 2
 * return the concatenated string from an array of substring
 * assign the result to variable 'str'
 * hint: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
 */
let str;
const substr = ['I', ' love', ' JS'];
str = substr[0] + substr[1] + substr[2];
/** Task: 3
 * calculate a total of income of certain person
 * assign the result to the variable 'totalIncome'
 * hint: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in
 */
let totalIncome;
const personIncomes = {
  salary: 1985,
  rent: -600,
  interestOnDeposit: 250,
  otherExpences: -300,
};
totalIncome = personIncomes.salary + personIncomes.rent + personIncomes.interestOnDeposit + personIncomes.otherExpences;

module.exports = {
  forFactorial,
  whileFactorial,
  doFactorial,
  str,
  totalIncome,
};
